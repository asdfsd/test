import logging
#import threading
import time
import sys

active_interval = int()
rest_interval = int()
rest_msg = "Rest for the next "
active_msg = "Time left to work "

if len(sys.argv) == 1:
    active_interval = int(input("How long are you going to work, in minutes: "))
    rest_interval = input("How long are you going to take a break, in minutes: ")
else:
    active_interval = int(sys.argv[1])
    rest_interval = int(sys.argv[2])

def thread_function(name, interval):
    logging.info("Thread %s: starting", name)
    #time.sleep(int(interval*60))
    time.sleep(int(active_interval))
    logging.info("Thread %s: finishing", name)

def countdown(interval, msg_t, mute=1):
    for i in range(interval, -1, -1):
        time.sleep(1)
        reminder_min = str(i//60).zfill(2)
        reminder_sec = str(i%60).zfill(2)
        msg = msg_t+reminder_min+":"+reminder_sec
        logging.info(msg)
        if not(mute):
            print("\a")

if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    #logging.info("Main    : before creating thread")
    #x = threading.Thread(target=thread_function, args=(1, interval))
    #x.start()

    #time.sleep(active_interval)
    #time.sleep(active_interval*60)
    countdown(active_interval*60, active_msg)
    countdown(rest_interval*60, rest_msg, 0)


    #msg = "It's time to take a rest"


    #logging.info("Main    : all done")
